#!/usr/bin/python3

import os
import fnmatch
import sys
import getopt

cmd_line_opts = {'print-binary': False}
all_aer_status_dict = {}
corr_err_status_reg = {"0x00000001": "Receiver Error Status",
                       "0x00000040": "Bad TLP Status",
                       "0x00000080": "Bad DLLP Status",
                       "0x00000100": "REPLAY_NUM Rollover Status",
                       "0x00001000": "Replay Timer Timeout Status",
                       "0x00002000": "Advisory Non-Fatal Error Status"}


def main():
    all_files = find('*.dmesg', '.') + find('dmesg*.txt', '.')
    file_aer_errors = {}

    for dmesg_file in all_files:
        file_aer_errors = get_aer_errors(dmesg_file)
        update_all_aer_dict(file_aer_errors)

    print_decoded_errors()

    print("There were", len(all_files), "*.dmesg files parsed")


def print_decoded_errors():
    all_error_counter = 0
    # print out the global aer counters
    for aer_status, aer_status_cnt in \
        sorted(all_aer_status_dict.get('all_aer_status').items(),
               key=lambda kv: (kv[1], kv[0]), reverse=True):
        error_strings = {}

        for error in corr_err_status_reg.keys():
            h_aer_status = int(aer_status, 16)
            h_error = int(error, 16)

            if (h_aer_status & h_error):
                if not error_strings:
                    error_strings[aer_status] = {
                        error: corr_err_status_reg.get(error)}
                else:
                    error_strings[aer_status][error] = corr_err_status_reg.get(
                        error)

        if cmd_line_opts.get('print-binary') is True:
            print(aer_status, '{0:016b}'.format(int(aer_status, 16)), 'occurrences',
                  '{:5d}'.format(aer_status_cnt), error_strings.get(aer_status))
        else:
            print(aer_status, 'occurrences', '{:5d}'.format(aer_status_cnt),
                  error_strings.get(aer_status))
        all_error_counter += aer_status_cnt

    print("There are", all_error_counter, "total errors found")

def update_all_aer_dict(file_aer_errors_dict):
    global all_aer_status_dict

    if 'all_aer_files' in all_aer_status_dict.keys():
        all_aer_status_dict['all_aer_files'].append(file_aer_errors_dict)
    else:
        all_aer_status_dict['all_aer_files'] = [file_aer_errors_dict]

    # {
    #   "all_aer_status": {
    #     "0x00000001": 1,
    #     "0x00000080": 1
    #   },
    #   "all_aer_files": [
    #     {
    #       "filename": "",
    #       "aer_status": {
    #         "0x00000001": 1,
    #         "0x00000080": 1
    #       }
    #     }
    #   ]
    # }

    for aer_status_code in file_aer_errors_dict['aer_status'].keys():
        f_aer_status_val = file_aer_errors_dict['aer_status'].get(
            aer_status_code)

        if 'all_aer_status' in all_aer_status_dict.keys():
            if not aer_status_code in all_aer_status_dict['all_aer_status'].keys():
                all_aer_status_dict['all_aer_status'][aer_status_code] = f_aer_status_val
            else:
                g_aer_status_val = all_aer_status_dict.get(
                    'all_aer_status').get(aer_status_code)
                all_aer_status_dict['all_aer_status'][aer_status_code] = g_aer_status_val + \
                    f_aer_status_val
        else:
            all_aer_status_dict['all_aer_status'] = {
                aer_status_code: f_aer_status_val}
    return()


def get_aer_errors(fname):
    file = open(fname, 'r')
    aer_status_dict = {'filename': fname, 'aer_status': {}}

    #     {
    #       "filename": "",
    #       "aer_status": {
    #         "0x00000001": 1,
    #         "0x00000080": 1
    #       }
    #     }

    for line in file.readlines():
        if 'aer_status:' in line.split():
            # this is not pythonic since we are parsing the same string twice
            # ['aer_status:', '0x00002001,', 'aer_mask:', '0x00000000']
            aer_status_code = line[line.find('aer_status:'):].split()[
                1].rstrip(',')

            if aer_status_code in aer_status_dict['aer_status'].keys():
                aer_status_dict['aer_status'][aer_status_code] += 1
            else:
                aer_status_dict['aer_status'][aer_status_code] = 1

    return(aer_status_dict)


def find(pattern, path):
    result = []

    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))

    return result


def print_cmdline_help():
    print("""--print-binary outputs binary decoded column

    """)


if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h", ["help", "print-binary"])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(err)  # will print something like "option -a not recognized"
        print_cmdline_help()
        sys.exit(2)
    output = None
    verbose = False
    for o, a in opts:
        if o in ("-h", "--help"):
            print_cmdline_help()
            sys.exit()
        elif o in ("--print-binary"):
            cmd_line_opts['print-binary'] = True
        else:
            assert False, "unhandled option"
    main()
