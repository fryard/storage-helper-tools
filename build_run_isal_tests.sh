#!/bin/bash

# git clone https://github.com/intel/isa-l.git
# sudo dnf install numactl -y

make clean
./autogen.sh
./configure
make -j `nproc` perfs

for testcase in crc/crc32_gzip_refl_perf \
            crc/crc32_ieee_perf \
            crc/crc32_iscsi_perf \
            raid/pq_gen_perf
  do
    # for i in `seq 8`
    for i in $(echo {15..0..4} {14..0..4} {13..0..4} {12..0..4} | cut -d' ' -f-8)
    do
      echo "Instance $i running $testcase now"
      numactl --physcpubind $i --localalloc $testcase &
      # taskset -c $i $testcase &
  done
  sleep 10
done
