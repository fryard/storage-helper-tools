#!/bin/bash

FILES=$(find . -type f | grep -e 'dmesg.*\.txt' -e' *.\.dmesg')

for error in $(grep --no-filename -Eo 'aer_status: 0x[0-9]{8}, aer_mask: 0x[0-9]{8}$' $FILES | sort | uniq)
do
  echo -n "checking $error "
  grep -o $error $FILES | wc -l
done

for error in $( grep --no-filename -Eo 'aer_status: 0x[0-9]{8}' | cut -d ' ' -f2 | sort | uniq )
do
  echo -n "Unique error code $error "
  grep -o $error $FILES | wc -l
done
