#!/usr/bin/python3

import os
import fnmatch
import json
import csv
import re
import hashlib
import sys
import collections

g_csv_file_list = []


def main():
    all_files = find('*.dmesg', '.') + find('dmesg*.txt', '.')

    for dmesg_file in all_files:
        parser = AerParser(slot_map_enable=True)
        d, e = parser.run(dmesg_file)
        del(parser)
        exporter = AerExporter(d, e, dmesg_file)

        # Don't create JSON, CSV or error files with empty dict
        if d:
            exporter.create_json_file()
            exporter.create_csv_file()
            exporter.create_error_file()
            exporter.create_mono_csv_file()
        del(exporter)


class AerExporter:
    def __init__(self, d, e, fname):
        self.dictionary = d
        self.s = e
        self.fname = fname

    def create_mono_csv_file(self):
        jumbo_csv_file = 'all_dmesg.csv'
        header = ''

        # Get the csv header from the first line in the first file
        with open(g_csv_file_list[0], 'r') as f:
            header = f.readline()

        with open(jumbo_csv_file, 'w') as fp:
            fp.write(header)
            for csv_file in g_csv_file_list:
                with open(csv_file, 'r') as f:
                    for line in f:
                        if not header in line:
                            fp.write(line)

    def create_json_file(self):
        json_file = self.fname + '.json'
        with open(json_file, 'w') as fp:
            json.dump(self.dictionary, fp)

    def create_csv_file(self):
        header_written = False
        csv_file = self.fname + '.csv'
        g_csv_file_list.append(csv_file)
        with open(csv_file, 'w') as f:
            for k in self.dictionary.keys():
                v = self.dictionary.get(k)
                writer = csv.DictWriter(f, v.keys())
                if not header_written:
                    header_written = True
                    writer.writeheader()
                writer.writerow(v)

    def create_error_file(self):
        error_file = self.fname + '.error'
        with open(error_file, "w") as text_file:
            text_file.write(self.s)


class AerParser:
    def __init__(self, slot_map_enable):
        self.d = {}
        self.fname = None
        self.fname_md5 = None
        self.slot_map_enable = slot_map_enable
        if self.slot_map_enable:
            self.slot_map = self.init_slot_map()

    def init_slot_map(self):
        if self.slot_map_enable:
            return({"0000:01:00.0": "2", "0000:02:00.0": "4", "0000:03:00.0": "6",
                    "0000:04:00.0": "8", "0000:05:00.0": "10", "0000:06:00.0": "12",
                    "0000:41:00.0": "1", "0000:42:00.0": "3", "0000:43:00.0": "5",
                    "0000:44:00.0": "7", "0000:45:00.0": "9", "0000:46:00.0": "11",
                    "0000:81:00.0": "23", "0000:82:00.0": "21", "0000:83:00.0": "19",
                    "0000:84:00.0": "17", "0000:85:00.0": "15", "0000:86:00.0": "13",
                    "0000:c8:00.0": "24", "0000:c9:00.0": "22", "0000:ca:00.0": "20",
                    "0000:cb:00.0": "18", "0000:cc:00.0": "16", "0000:cd:00.0": "14",
                    "0000:c2:00.0": "24", "0000:c3:00.0": "22", "0000:c4:00.0": "20",
                    "0000:c5:00.0": "18", "0000:c6:00.0": "16", "0000:c7:00.0": "14"})

    def init_error_dict(self):
        self.d = collections.OrderedDict()
        # Error section
        self.d['error_timestamp'] = ''
        self.d['error_cnt'] = ''
        self.d['error_name'] = ''
        self.d['error_idx'] = ''
        self.d['fname_md5'] = ''
        self.d['fname'] = ''
        self.d['lineno'] = ''
        self.d['error_type'] = ''
        self.d['fru_text'] = ''
        self.d['section_type'] = ''
        self.d['port_type_num'] = ''
        self.d['port_type'] = ''
        self.d['version'] = ''
        self.d['command'] = ''
        self.d['opcode'] = ''
        self.d['status'] = ''
        self.d['vendor'] = ''
        self.d['vendor_id'] = ''
        self.d['device_id'] = ''
        self.d['dev_id'] = ''
        if self.slot_map_enable:
            self.d['slot_num'] = ''
        self.d['slot'] = ''
        self.d['secondary_bus'] = ''
        self.d['class_code'] = ''
        self.d['secondary_name'] = ''
        self.d['secondary_status'] = ''
        self.d['secondary_control'] = ''
        # AER section
        self.d['aer_timestamp'] = ''
        self.d['origin'] = ''
        self.d['aer_status'] = ''
        self.d['aer_mask'] = ''
        self.d['aer_status_bit'] = []
        self.d['aer_status_message'] = []
        self.d['aer_layer'] = ''
        self.d['aer_agent'] = ''
        self.d['aer_dev_id'] = ''
        self.d['err_section_cnt'] = ''
        self.d['aer_section_cnt'] = ''
        self.d['matched_aer_section_cnt'] = ''

    def get_fname_md5sum(self, fname):
        self.fname_md5 = hashlib.md5(open(fname, 'rb').read()).hexdigest()

    def run(self, fname):
        self.fname = fname
        self.get_fname_md5sum(fname)
        file = open(self.fname, 'r')
        processing_errors = ""
        # Reference: Implementation Details Section 1
        multi_errror_list = []
        multi_aer_list = []
        error_dict = {}
        aer_dict = {}
        g_error_dict = {}
        aer_cnt_found = 0
        matched_aer_cnt_found = 0
        err_cnt_found = 0
        prev_aer_rowkey = None
        aer_rowkey = None
        rowkey = None
        prev_rowkey = None
        lineno = 0
        aer_regex = r'^\[ *[0-9]+\.[0-9]+\] .* [0-9]+:[a-z0-9]+:[a-z0-9]+\.[0-9]+((: aer_status:)|(: AER:)|(: aer_layer))'
        old_aer_regex = r'^\[ *[0-9]+\.[0-9]+\] ((Bad TLP)|(Receiver Error)|(Bad DLLP)|(Rollover)|(Timeout)|(Non Fatal Error))'

        for line in file.readlines():
            lineno += 1
            currentline = line.strip()
            if re.match(r'^\[ *[0-9]+\.[0-9]+\] \{[0-9]+\}\[Hardware Error\]:', currentline):
                # Matched log section --> [ 8000.000000] {600}[Hardware Error]:
                if ':  Error ' in currentline:
                    # Matched log section --> :  Error
                    self.init_error_dict()
                    error_dict = self.d
                    error_dict.update(self.parse_hardware_error(currentline))
                    error_dict['lineno'] = lineno

                    # Started a new Error # section
                    if aer_cnt_found:  # Finished processing an aer section
                        err_cnt_found = 1
                        aer_dict = {}
                        aer_cnt_found = matched_aer_cnt_found = 0
                        multi_aer_list = []
                        multi_errror_list = [error_dict]
                    else:  # Looping on Error 0, Error 1, ...
                        err_cnt_found += 1

                    if err_cnt_found == 1:
                        # First error element in log may not be Error 0 so use err_cnt_found
                        # instead of error_idx (i.e. Error #) which could be anything
                        # Found next Error # also means not processing AER sections
                        multi_errror_list = [error_dict]
                    else:
                        multi_errror_list.append(error_dict)

                    prev_rowkey = rowkey
                    rowkey = error_dict.get('error_timestamp') + ':' + error_dict.get(
                        'error_cnt') + ':' + error_dict.get('error_idx') + ':' + self.fname_md5
                    g_error_dict[rowkey] = error_dict
                    g_error_dict[rowkey]['fname_md5'] = self.fname_md5
                    g_error_dict[rowkey]['fname'] = self.fname
                    g_error_dict[rowkey].update(
                        self.parse_hardware_error_error(currentline))
                elif rowkey is not prev_rowkey:
                    # Ignoring truncated log sections
                    if ':  fru_text' in currentline:
                        g_error_dict[rowkey].update(
                            self.parse_hardware_error_fru(currentline))
                    elif ':   section_type' in currentline:
                        g_error_dict[rowkey].update(
                            self.parse_hardware_error_section_type(currentline))
                    elif ':   port_type:' in currentline:
                        g_error_dict[rowkey].update(
                            self.parse_hardware_error_port_type(currentline))
                    elif ':   version:' in currentline:
                        g_error_dict[rowkey].update(
                            self.parse_hardware_error_version(currentline))
                    elif ':   command:' in currentline:
                        g_error_dict[rowkey].update(
                            self.parse_hardware_error_command(currentline))
                    elif ':   device_id:' in currentline:
                        g_error_dict[rowkey].update(
                            self.parse_hardware_error_device_id(currentline))
                    elif ':   slot:' in currentline:
                        g_error_dict[rowkey].update(
                            self.parse_hardware_error_slot(currentline))
                    elif ':   secondary_bus:' in currentline:
                        g_error_dict[rowkey].update(
                            self.parse_hardware_error_secondary_bus(currentline))
                    elif ':   vendor_id:' in currentline:
                        g_error_dict[rowkey].update(
                            self.parse_hardware_error_vendor_device(currentline))
                    elif ':   class_code:' in currentline:
                        g_error_dict[rowkey].update(
                            self.parse_hardware_error_class_code(currentline))
                    elif ':   bridge:' in currentline:
                        g_error_dict[rowkey].update(
                            self.parse_hardware_error_bridge(currentline))
                    else:
                        msg = self.fname + '(' + str(lineno) + \
                            ') ERROR CAN\'T DECODE --> ' + currentline
                        processing_errors += msg
                        print(msg)
                else:
                    msg = self.fname + \
                        '(' + str(lineno) + \
                        ') Skipping truncated errors not starting with \'[Hardware Error]:  Error #\''
                    processing_errors += msg
                    print(msg)
            # Implementation Details Section 2
            elif re.match(aer_regex, currentline) or re.match(old_aer_regex, currentline):
                # Implementation Details Section 3
                if err_cnt_found:

                    if ' aer_status:' in currentline:
                        aer_cnt_found += 1

                    if matched_aer_cnt_found >= err_cnt_found and ' aer_status:' in currentline:
                        # Clear out aer_dict to know that we skipped one in the elif case below
                        aer_dict = {}

                        msg = self.fname + '(' + str(lineno) + \
                            ') More AER sections found and matched than Error # sections --> ' + currentline
                        processing_errors += msg
                        print(msg)
                    elif matched_aer_cnt_found < err_cnt_found and ' aer_status:' in currentline:
                        _timestamp = multi_errror_list[matched_aer_cnt_found].get(
                            'error_timestamp')
                        _err_cnt = multi_errror_list[matched_aer_cnt_found].get(
                            'error_cnt')
                        _err_idx = multi_errror_list[matched_aer_cnt_found].get(
                            'error_idx')
                        _err_dev_id = multi_errror_list[matched_aer_cnt_found].get(
                            'dev_id')

                        prev_aer_rowkey = aer_rowkey
                        aer_rowkey = _timestamp + ':' + _err_cnt + ':' + _err_idx + ':' + self.fname_md5

                        g_error_dict[aer_rowkey]['err_section_cnt'] = err_cnt_found
                        g_error_dict[aer_rowkey]['aer_section_cnt'] = aer_cnt_found

                        aer_dict = self.parse_aer_error(currentline)
                        aer_dict.update(
                            self.parse_aer_error_status_mask(currentline))

                        if aer_dict.get('aer_dev_id') == _err_dev_id:
                            if not prev_aer_rowkey or aer_rowkey == prev_aer_rowkey:
                                # Processing a new error/AER row
                                matched_aer_cnt_found = 1
                            else:
                                matched_aer_cnt_found += 1

                            g_error_dict[aer_rowkey].update(aer_dict)
                            aer_dict['aer_rowkey'] = aer_rowkey
                            multi_aer_list.append(aer_dict)
                        else:
                            # Clear out aer_dict to know that we skipped one in the elif case below
                            aer_dict = {}
                            # Don't insert this data and don't increment matched_aer_cnt_found to allow
                            # conditional above to process if more AER sections than Error # sections
                            msg = self.fname + \
                                '(' + str(lineno) + \
                                ') Skipping mismatched aer_dev_id --> ' + currentline
                            processing_errors += msg
                            print(msg)

                        g_error_dict[aer_rowkey]['matched_aer_section_cnt'] = matched_aer_cnt_found
                    # Implementation Details Section 4
                    elif aer_dict and matched_aer_cnt_found <= err_cnt_found:
                        if ': AER:    [' in currentline or re.match(old_aer_regex, currentline):
                            _d = self.parse_aer_error_aer_status_bit(line)
                            # n number of status bits in the array ['0', '12']
                            if 'aer_status_bit' in g_error_dict[aer_rowkey].keys():
                                g_error_dict[aer_rowkey]['aer_status_bit'].append(
                                    _d.get('aer_status_bit'))
                            else:
                                g_error_dict[aer_rowkey]['aer_status_bit'] = [
                                    _d.get('aer_status_bit')]

                            # n number of status messages in the array ['RxErr', 'Timeout']
                            if 'aer_status_message' in g_error_dict[aer_rowkey].keys():
                                g_error_dict[aer_rowkey]['aer_status_message'].append(
                                    _d.get('aer_status_message'))
                            else:
                                g_error_dict[aer_rowkey]['aer_status_message'] = [
                                    _d.get('aer_status_message')]
                        elif ': aer_layer' in currentline:
                            g_error_dict[aer_rowkey].update(
                                self.parse_aer_error_aer_layer(currentline))
                        else:
                            msg = self.fname + '(' + str(lineno) + \
                                ') AER CAN\'T DECODE --> ' + currentline
                            processing_errors += msg
                            print(msg)

                    if ' aer_status:' in currentline:
                        self.update_multi_aer_list_counters(
                            multi_aer_list, g_error_dict, err_cnt_found, aer_cnt_found)
                else:
                    # Implementation Details Section 5
                    msg = self.fname + \
                        '(' + str(lineno) + \
                        ') Skipping AER section found with no starting Error # section'
                    processing_errors += msg
                    print(msg)
        return(g_error_dict, processing_errors)

    def update_multi_aer_list_counters(self, l, d, e, a):
        # multi_aer_list, g_error_dict, err_cnt_found, aer_cnt_found
        # Set the counters for the AER sections on the error
        # rows we just processed.  You won't know this until
        # you are done processing AER sections and you start
        # processing Error # sections again so do it each time
        # to stay current
        for aerdict in l:
            _aer_rowkey = aerdict.get('aer_rowkey')
            d[_aer_rowkey]['err_section_cnt'] = e
            d[_aer_rowkey]['aer_section_cnt'] = a

    def parse_hardware_error(self, s):
        # [ 8701.632571] {671}[Hardware Error]:
        d = {}
        d['error_timestamp'] = re.match(
            r'^\[ *[0-9]+\.[0-9]+\]', s).group().strip('[]')
        d['error_cnt'] = re.sub(
            r'[\{\}]', '', re.search(r'\{[0-9]+\}', s).group())
        d['error_name'] = 'Hardware Error'
        d['error_idx'] = re.sub(
            '[Error ,]', '', re.search('Error [0-9]+,', s).group())
        return(d)

    def parse_hardware_error_error(self, s):
        # [ 8701.632571] {671}[Hardware Error]:  Error 8, type: corrected
        d = {}
        d['error_type'] = re.search('type: .*$', s).group().split()[1]
        return(d)

    def parse_hardware_error_fru(self, s):
        # [    2.739010] {1}[Hardware Error]:  fru_text: PcieError
        d = {}
        d['fru_text'] = re.search(
            'fru_text: .*$', s).group().split(':')[1].strip()
        return(d)

    def parse_hardware_error_section_type(self, s):
        # [ 8701.632572] {671}[Hardware Error]:   section_type: PCIe error
        d = {}
        d['section_type'] = re.search(
            'section_type: .*$', s).group().split(':')[1].lstrip()
        return(d)

    def parse_hardware_error_port_type(self, s):
        # [ 8701.632572] {671}[Hardware Error]:   port_type: 4, root port
        d = {}
        offset = s.find('port_type:')
        d['port_type_num'] = s[offset:].split(',')[0].split()[1]
        d['port_type'] = s[offset:].split(',')[1].strip()
        return(d)

    def parse_hardware_error_version(self, s):
        # [ 8701.632573] {671}[Hardware Error]:   version: 0.2
        d = {}
        d['version'] = re.search(
            'version: .*$', s).group().split(':')[1].strip()
        return(d)

    def parse_hardware_error_command(self, s):
        # [ 8701.632574] {671}[Hardware Error]:   command: 0x0407, status: 0x0010
        d = {}
        d['command'] = re.search(
            'command: .*$', s).group().split(',')[0].split(':')[1].strip()
        d['status'] = re.search(
            'command: .*$', s).group().split(',')[1].split(':')[1].strip()
        d['opcode'] = self.decode_command(d.get('command'))
        return(d)

    def parse_hardware_error_device_id(self, s):
        # [ 8701.632575] {671}[Hardware Error]:   device_id: 0000:00:01.3
        d = {}
        d['dev_id'] = s[s.find('device_id:'):].split(':', 1)[1].strip()
        # Static mapping of device numbers to slot numbers
        if self.slot_map_enable:
            d['slot_num'] = self.slot_map.get(d.get('dev_id'))
        return(d)

    def parse_hardware_error_slot(self, s):
        # [ 8701.632575] {671}[Hardware Error]:   slot: 4
        d = {}
        d['slot'] = s[s.find('slot:'):].split(':', 1)[1].strip()
        return(d)

    def parse_hardware_error_secondary_bus(self, s):
        # [ 8701.632576] {671}[Hardware Error]:   secondary_bus: 0x03
        d = {}
        d['secondary_bus'] = s[s.find('secondary_bus:'):].split(':', 1)[
            1].strip()
        return(d)

    def parse_hardware_error_vendor_device(self, s):
        # [ 8701.632577] {671}[Hardware Error]:   vendor_id: 0x1022, device_id: 0x1483
        d = {}
        d['vendor_id'] = re.search(
            'vendor_id: .*$', s).group().split(',')[0].split(':')[1].strip()
        d['device_id'] = re.search(
            'vendor_id: .*$', s).group().split(',')[1].split(':')[1].strip()
        d['vendor'] = self.decode_vendor_id(d.get('vendor_id'))
        return(d)

    def parse_hardware_error_class_code(self, s):
        # [ 8701.632577] {671}[Hardware Error]:   class_code: 060400
        d = {}
        d['class_code'] = s[s.find('class_code:'):].split(':', 1)[1].strip()
        return(d)

    def parse_hardware_error_bridge(self, s):
        # [ 8701.632578] {671}[Hardware Error]:   bridge: secondary_status: 0x0000, control: 0x0012
        d = {}
        d['secondary_name'] = 'bridge'
        d['secondary_status'] = s[s.find('bridge:'):].split(
            ':', 1)[1].strip().split(',')[0].split(':')[1].strip()
        d['secondary_control'] = s[s.find('bridge:'):].split(
            ':', 1)[1].strip().split(',')[1].split(':')[1].strip()
        return(d)

    def parse_aer_error(self, s):
        # New style AER string
        # [ 8701.632668] pcieport 0000:00:01.3: AER:
        # [ 2199.922998] mlx5_core 0000:09:00.0: AER:
        # Old style no AER: in string
        # [33283.874633] pcieport 0000:00:01.6: aer_status:
        d = {}
        origin_regex = r'^\[ *[0-9]+\.[0-9]+\] [a-zA-Z0-9_]* [0-9]{4}:[a-z0-9]{2}:[a-z0-9]{2}\.[0-9]{1}: ((AER:)|(aer_status:))'
        aer_dev_id_regex = r'[0-9]{4}:[a-z0-9]{2}:[a-z0-9]{2}\.[0-9]{1}'
        d['aer_timestamp'] = re.match(
            r'^\[ *[0-9]+\.[0-9]+\]', s).group().strip('[]')
        d['origin'] = re.match(origin_regex, s).group().split(']')[
            1].strip().split()[0]
        d['aer_dev_id'] = re.search(aer_dev_id_regex, s).group()
        return(d)

    def parse_aer_error_status_mask(self, s):
        # New style AER string
        # [ 8701.632668] pcieport 0000:00:01.3: AER: aer_status: 0x00000040, aer_mask: 0x00000000
        # Old style no AER: in string
        # [33283.874633] pcieport 0000:00:01.6: aer_status: 0x00000040, aer_mask: 0x00000000
        # [33820.949490] nvme 0000:03:00.0: aer_status: 0x00000001, aer_mask: 0x00000000
        d = {}
        d['aer_status'] = s[s.find('aer_status'):].split(',')[
            0].split(':')[1].strip()
        d['aer_mask'] = s[s.find('aer_mask'):].split(':')[1].strip()
        return(d)

    def parse_aer_error_aer_status_bit(self, s):
        d = {}
        if ': AER:' in s:
            # [ 8701.632676] pcieport 0000:00:01.3: AER:    [ 6] BadTLP
            d['aer_status_bit'] = s[s.find(': AER:    ['):].split(']')[
                0].split('[')[1].strip()
            d['aer_status_message'] = s[s.find(': AER:    ['):].split(']')[
                1].strip()
        else:
            # [33283.882106] Bad TLP
            d['aer_status_message'] = s.split(']')[1].strip()

            if ('Receiver Error' or 'RxErr') in d['aer_status_message']:
                d['aer_status_bit'] = '0'
            elif ('Bad TLP' or 'BadTLP') in d['aer_status_message']:
                d['aer_status_bit'] = '6'
            elif 'Bad DLLP' or 'BadDLLP' in d['aer_status_message']:
                d['aer_status_bit'] = '7'
            elif 'Rollover' in d['aer_status_message']:
                d['aer_status_bit'] = '8'
            elif 'Timeout' in d['aer_status_message']:
                d['aer_status_bit'] = '12'
            elif 'NonFatalErr' in d['aer_status_message']:
                d['aer_status_bit'] = '13'
            else:
                d['aer_status_bit'] = ''
        return(d)

    def parse_aer_error_aer_layer(self, s):
        # New style AER string
        # [ 8701.632686] pcieport 0000:00:01.3: AER: aer_layer=Data Link Layer, aer_agent=Receiver ID
        # Old style no AER: in string
        # [33283.882109] pcieport 0000:00:01.6: aer_layer=Data Link Layer, aer_agent=Receiver ID
        d = {}
        d['aer_layer'] = s[s.find('aer_layer'):].split(',')[
            0].split('=')[1].strip()
        d['aer_agent'] = s[s.find('aer_layer'):].split(',')[
            1].split('=')[1].strip()
        return(d)

    def decode_command(self, opcode):
        results = "UNKNOWN"
        opcode_dict = {"0x0406": "VPD Read", "0x0406": "VPD Write", "0x0407": "Reset"}
        if opcode in opcode_dict.keys():
            results = opcode_dict.get(opcode)
        return results

    def decode_vendor_id(self, vendor_id):
        results = "UNKNOWN"
        vendor_id_dict = {"0x1022": "Advanced Micro Devices, Inc. [AMD]", "0x144d": "Samsung Electronics Co Ltd"}
        if vendor_id in vendor_id_dict.keys():
            results = vendor_id_dict.get(vendor_id)
        return results

def find(pattern, path):
    result = []

    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))

    return result


if __name__ == "__main__":
    main()

