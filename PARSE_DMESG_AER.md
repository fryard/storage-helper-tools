# storage-helper-tools/parse_dmesg_aer.py

```
parse_dmesg_aer.py
0x00000001 occurrences  9785 {'0x00000001': 'Receiver Error Status'}
0x00000040 occurrences  1437 {'0x00000040': 'Bad TLP Status'}
0x00002001 occurrences   280 {'0x00000001': 'Receiver Error Status', '0x00002000': 'Advisory Non-Fatal Error Status'}
0x00000080 occurrences    28 {'0x00000080': 'Bad DLLP Status'}
0x00001040 occurrences    20 {'0x00000040': 'Bad TLP Status', '0x00001000': 'Replay Timer Timeout Status'}
0x00001000 occurrences    11 {'0x00001000': 'Replay Timer Timeout Status'}
0x00001100 occurrences     2 {'0x00000100': 'REPLAY_NUM Rollover Status', '0x00001000': 'Replay Timer Timeout Status'}
0x00002081 occurrences     1 {'0x00000001': 'Receiver Error Status', '0x00000080': 'Bad DLLP Status', '0x00002000': 'Advisory Non-Fatal Error Status'}
0x000010c0 occurrences     1 {'0x00000040': 'Bad TLP Status', '0x00000080': 'Bad DLLP Status', '0x00001000': 'Replay Timer Timeout Status'}
0x00000081 occurrences     1 {'0x00000001': 'Receiver Error Status', '0x00000080': 'Bad DLLP Status'}
There are 11566 total errors found
There were 180 *.dmesg files parsed
```

```
parse_dmesg_aer.py --print-binary
0x00000001 0000000000000001 occurrences  9785 {'0x00000001': 'Receiver Error Status'}
0x00000040 0000000001000000 occurrences  1437 {'0x00000040': 'Bad TLP Status'}
0x00002001 0010000000000001 occurrences   280 {'0x00000001': 'Receiver Error Status', '0x00002000': 'Advisory Non-Fatal Error Status'}
0x00000080 0000000010000000 occurrences    28 {'0x00000080': 'Bad DLLP Status'}
0x00001040 0001000001000000 occurrences    20 {'0x00000040': 'Bad TLP Status', '0x00001000': 'Replay Timer Timeout Status'}
0x00001000 0001000000000000 occurrences    11 {'0x00001000': 'Replay Timer Timeout Status'}
0x00001100 0001000100000000 occurrences     2 {'0x00000100': 'REPLAY_NUM Rollover Status', '0x00001000': 'Replay Timer Timeout Status'}
0x00002081 0010000010000001 occurrences     1 {'0x00000001': 'Receiver Error Status', '0x00000080': 'Bad DLLP Status', '0x00002000': 'Advisory Non-Fatal Error Status'}
0x000010c0 0001000011000000 occurrences     1 {'0x00000040': 'Bad TLP Status', '0x00000080': 'Bad DLLP Status', '0x00001000': 'Replay Timer Timeout Status'}
0x00000081 0000000010000001 occurrences     1 {'0x00000001': 'Receiver Error Status', '0x00000080': 'Bad DLLP Status'}
There are 11566 total errors found
There were 180 *.dmesg files parsed
```
