#!/usr/bin/python3

import os
import fnmatch
import json
import csv
import re
import hashlib
import sys
import collections

g_csv_file_list = []


def main():
    all_files = find('*.dmesg', '.') + find('dmesg*.txt', '.')

    for dmesg_file in all_files:
        parser = AerParser(slot_map_enable=True)
        d, e = parser.run(dmesg_file)
        del(parser)
        exporter = AerExporter(d, e, dmesg_file)

        # Don't create JSON, CSV or error files with empty dict
        if d:
            exporter.create_json_file()
            exporter.create_csv_file()
            exporter.create_error_file()
            exporter.create_mono_csv_file()
        del(exporter)


class AerExporter:
    def __init__(self, d, e, fname):
        self.dictionary = d
        self.s = e
        self.fname = fname

    def create_mono_csv_file(self):
        jumbo_csv_file = 'all_dmesg.csv'
        header = ''

        # Get the csv header from the first line in the first file
        with open(g_csv_file_list[0], 'r') as f:
            header = f.readline()

        with open(jumbo_csv_file, 'w') as fp:
            fp.write(header)
            for csv_file in g_csv_file_list:
                with open(csv_file, 'r') as f:
                    for line in f:
                        if not header in line:
                            fp.write(line)

    def create_json_file(self):
        json_file = self.fname + '.json'
        with open(json_file, 'w') as fp:
            json.dump(self.dictionary, fp)

    def create_csv_file(self):
        header_written = False
        csv_file = self.fname + '.csv'
        g_csv_file_list.append(csv_file)
        with open(csv_file, 'w') as f:
            for k in self.dictionary.keys():
                v = self.dictionary.get(k)
                writer = csv.DictWriter(f, v.keys())
                if not header_written:
                    header_written = True
                    writer.writeheader()
                writer.writerow(v)

    def create_error_file(self):
        if self.s:
            error_file = self.fname + '.error'
            with open(error_file, "w") as text_file:
                text_file.write(self.s)


class AerParser:
    def __init__(self, slot_map_enable):
        self.d = {}
        self.fname = None
        self.fname_md5 = None
        self.slot_map = {   # Endpoint mapping for sled 0:
                            "0000:01:00.0": "2",
                            "0000:02:00.0": "4",
                            "0000:03:00.0": "6",
                            "0000:04:00.0": "8",
                            "0000:05:00.0": "10",
                            "0000:06:00.0": "12",
                            "0000:41:00.0": "1",
                            "0000:42:00.0": "3",
                            "0000:43:00.0": "5",
                            "0000:44:00.0": "7",
                            "0000:45:00.0": "9",
                            "0000:46:00.0": "11",
                            "0000:81:00.0": "23",
                            "0000:82:00.0": "21",
                            "0000:83:00.0": "19",
                            "0000:84:00.0": "17",
                            "0000:85:00.0": "15",
                            "0000:86:00.0": "13",
                            "0000:c2:00.0": "24",
                            "0000:c3:00.0": "22",
                            "0000:c4:00.0": "20",
                            "0000:c5:00.0": "18",
                            "0000:c6:00.0": "16",
                            "0000:c7:00.0": "14",

                            # Additional endpoint mapping for sled 1 differences:
                            "0000:c8:00.0": "24",
                            "0000:c9:00.0": "22",
                            "0000:ca:00.0": "20",
                            "0000:cb:00.0": "18",
                            "0000:cc:00.0": "16",
                            "0000:cd:00.0": "14",

                            # Even more endpoint mapping for CRB:
                            "0000:e1:00.0": "NVME_0",
                            "0000:e2:00.0": "NVME_1",
                            "0000:e3:00.0": "NVME_2",
                            "0000:e4:00.0": "NVME_3",

                            # Root complex mapping:
                            "0000:00:01.1": "2",
                            "0000:00:01.2": "4",
                            "0000:00:01.3": "6",
                            "0000:00:01.4": "8",
                            "0000:00:01.5": "10",
                            "0000:00:01.6": "12",
                            "0000:40:03.1": "1",
                            "0000:40:03.2": "3",
                            "0000:40:03.3": "5",
                            "0000:40:03.4": "7",
                            "0000:40:03.5": "9",
                            "0000:40:03.6": "11",
                            "0000:80:01.1": "23",
                            "0000:80:01.2": "21",
                            "0000:80:01.3": "19",
                            "0000:80:01.4": "17",
                            "0000:80:01.5": "15",
                            "0000:80:01.6": "13",
                            "0000:c0:03.1": "24",
                            "0000:c0:03.2": "22",
                            "0000:c0:03.3": "20",
                            "0000:c0:03.4": "18",
                            "0000:c0:03.5": "16",
                            "0000:c0:03.6": "14",

                            # Root complex mapping for CRB:
                            "0000:e0:03.1": "NVME_0",
                            "0000:e0:03.2": "NVME_1",
                            "0000:e0:03.3": "NVME_2",
                            "0000:e0:03.4": "NVME_3"}

    def init_error_dict(self):
        self.d = collections.OrderedDict()
        # Error section
        self.d['error_timestamp'] = ''
        self.d['error_name'] = ''
        self.d['fname_md5'] = ''
        self.d['fname'] = ''
        self.d['lineno'] = ''
        self.d['error_type'] = ''
        self.d['dev_id_1'] = ''
        self.d['dev_id_2'] = ''
        self.d['dev_type'] = ''
        self.d['device'] = ''
        self.d['error_slot_num_1'] = ''
        self.d['error_slot_num_2'] = ''
        # AER section
        self.d['aer_dev_id'] = []
        self.d['aer_slot_num'] = []
        self.d['aer_severity'] = ''
        self.d['aer_type'] = ''
        self.d['aer_type_ext'] = ''
        self.d['aer_status'] = ''
        self.d['aer_mask'] = ''
        self.d['aer_status_bit'] = []
        self.d['aer_status_message'] = []

    def get_fname_md5sum(self, fname):
        self.fname_md5 = hashlib.md5(open(fname, 'rb').read()).hexdigest()

    def run(self, fname):
        self.fname = fname
        self.get_fname_md5sum(fname)
        file = open(self.fname, 'r')
        processing_errors = ""
        # Reference: Implementation Details Section 1
        error_dict = {}
        g_error_dict = {}
        err_cnt_found = 0
        rowkey = None
        lineno = 0
        device_section_found = 0
        prev_rowkey = None
        prev_prev_rowkey = None

# [ 6876.924384] pcieport 0000:c0:03.3: AER: Corrected error received: 0000:c4:00.0
# [ 6876.924392] nvme 0000:c4:00.0: AER: PCIe Bus Error: severity=Corrected, type=Physical Layer, (Receiver ID)
# [ 6876.934880] nvme 0000:c4:00.0: AER:   device [144d:a824] error status/mask=00000001/00000000
# [ 6876.943794] nvme 0000:c4:00.0: AER:    [ 0] RxErr

        currentline = None
        for line in file.readlines():
            lineno += 1
            currentline = line.strip()
            _d = {}
            if re.search(': AER:', currentline):
                if re.search(': AER: (?:Multiple )?Corrected error received', currentline):
                    device_section_found = 0
                    self.init_error_dict()
                    err_cnt_found += 1
                    error_dict = self.d
                    error_dict['lineno'] = lineno
                    error_dict.update(
                        self.parse_starting_aer_error(currentline))
                    prev_prev_rowkey = prev_rowkey
                    prev_rowkey = rowkey
                    rowkey = str(
                        lineno) + ':' + error_dict.get('error_timestamp') + ':' + self.fname_md5
                    g_error_dict[rowkey] = error_dict
                    g_error_dict[rowkey]['fname_md5'] = self.fname_md5
                    g_error_dict[rowkey]['fname'] = self.fname
                elif re.search(': AER: PCIe', currentline):
                    _d = self.parse_aer_bus_error(currentline)
                    if _d.get('dev_id_2') != error_dict.get('dev_id_2'):
                        msg = self.fname + \
                            ' +' + \
                            str(lineno) + ' ERROR: DEV IDs ' + _d.get('dev_id_2') + \
                            ' <-> ' + \
                            error_dict.get('dev_id_2') + ' don\'t match\n'
                        processing_errors += msg
                        print(msg)
                        if g_error_dict.get(prev_rowkey).get('dev_id_2') == _d['dev_id_2']:
                            msg = self.fname + \
                                ' +' + \
                                str(lineno) + ' belongs with error on line ' + \
                                str(g_error_dict.get(
                                    prev_rowkey).get('lineno')) + '\n'
                            processing_errors += msg
                            print(msg)
                            g_error_dict.get(prev_rowkey).update(_d)
                        else:
                            print('ERROR1: THIS DOES NOT WORK!')
                            sys.exit()
                    else:
                        error_dict.update(_d)
                elif re.search(': AER:   device', currentline):
                    # Found multiple device sections within one error section.
                    # Note the last one found is what is kept.
                    # [14348.456015] nvme 0000:c4:00.0: AER: PCIe Bus Error: severity=Corrected, type=Physical Layer, (Receiver ID)
                    # [14348.456997] nvme 0000:84:00.0: AER:   device [144d:a824] error status/mask=00000001/00000000
                    # [14348.465662] nvme 0000:c4:00.0: AER:   device [144d:a824] error status/mask=00000001/00000000
                    if device_section_found != 0:
                        msg = self.fname + \
                            ' +' + \
                            str(lineno) + \
                            ' ERROR: found multiple device sections\n'
                        processing_errors += msg
                        print(msg)
                    else:
                        device_section_found += 1
                    _d = self.parse_starting_aer_error_status(currentline)
                    if _d.get('dev_id_2') != error_dict.get('dev_id_2'):
                        msg = self.fname + \
                            ' +' + \
                            str(lineno) + ' ERROR: DEV IDs ' + _d.get('dev_id_2') + \
                            ' <-> ' + \
                            error_dict.get('dev_id_2') + ' don\'t match\n'
                        processing_errors += msg
                        print(msg)
                        if g_error_dict.get(prev_rowkey).get('dev_id_2') == _d['dev_id_2']:
                            msg = self.fname + \
                                ' +' + \
                                str(lineno) + ' belongs with error on line ' + \
                                str(g_error_dict.get(
                                    prev_rowkey).get('lineno')) + '\n'
                            processing_errors += msg
                            print(msg)
                            g_error_dict.get(prev_rowkey).update(_d)
                        else:
                            print('ERROR2: THIS DOES NOT WORK!')
                            sys.exit()
                    else:
                        error_dict.update(_d)
                elif re.search(': AER:    ', currentline):
                    # Can be multiple lines of these
                    _d = self.parse_aer_bitfield(currentline)

                    if _d.get('dev_id_2') != error_dict.get('dev_id_2'):
                        msg = self.fname + \
                            ' +' + \
                            str(lineno) + ' ERROR: DEV IDs ' + _d.get('dev_id_2') + \
                            ' <-> ' + \
                            error_dict.get('dev_id_2') + ' don\'t match\n'
                        processing_errors += msg
                        print(msg)
                        if g_error_dict.get(prev_rowkey).get('dev_id_2') == _d['dev_id_2']:
                            msg = self.fname + \
                                ' +' + \
                                str(lineno) + ' belongs with error on line ' + \
                                str(g_error_dict.get(
                                    prev_rowkey).get('lineno')) + '\n'
                            processing_errors += msg
                            print(msg)
                            g_error_dict.get(prev_rowkey).update(_d)
                        else:
                            if g_error_dict.get(prev_prev_rowkey).get('dev_id_2') == _d['dev_id_2']:
                                msg = self.fname + \
                                    ' +' + \
                                    str(lineno) + ' belongs with error on line ' + \
                                    str(g_error_dict.get(
                                        prev_prev_rowkey).get('lineno')) + '\n'
                                processing_errors += msg
                                print(msg)
                                g_error_dict.get(prev_prev_rowkey).update(_d)
                            else:
                                print('ERROR3: THIS DOES NOT WORK!', lineno)
                                sys.exit()
                    elif error_dict.get('aer_status_bit'):
                        error_dict.get('aer_status_bit').append(
                            _d.get('aer_status_bit')[0])
                        error_dict.get('aer_status_message').append(
                            _d.get('aer_status_message')[0])
                        if error_dict.get('dev_type') != _d.get('dev_type'):
                            msg = self.fname + \
                                ' +' + \
                                str(lineno) + \
                                ' AER dev_type does not match previous dev_type\n'
                            processing_errors += msg
                            print(msg)
                            sys.exit()
                        else:
                            error_dict['dev_type'] = _d.get('dev_type')
                        error_dict.get('aer_dev_id').append(
                            _d.get('aer_dev_id')[0])
                        error_dict.get('aer_slot_num').append(
                            _d.get('aer_slot_num')[0])
                    else:
                        error_dict.update(_d)
                else:
                    msg = self.fname + \
                        ' +' + str(lineno) + \
                        ' ERROR: not implemented' + currentline + '\n'
                    processing_errors += msg
                    print(msg)
                g_error_dict[rowkey] = error_dict
        return(g_error_dict, processing_errors)

    def parse_aer_bus_error(self, s):
        # [ 6876.924392] nvme 0000:c4:00.0: AER: PCIe Bus Error: severity=Corrected, type=Physical Layer, (Receiver ID)
        d = {}
        _s = re.search(r'PCIe Bus Error:.*', s).group().split(':')[1].strip()
        d['aer_severity'] = _s.split(',')[0].split('=')[1]
        d['aer_type'] = _s.split(',')[1].split('=')[1].strip()
        d['aer_type_ext'] = _s.split(',')[2].strip().strip('(').strip(')')
        d['dev_id_2'] = re.search(
            r'[a-z0-9]{4}:[a-z0-9]{2}:[a-z0-9]{2}\.[a-z0-9]{1}', s).group()
        return(d)

    def parse_starting_aer_error_status(self, s):
        # [ 6876.934880] nvme 0000:c4:00.0: AER:   device [144d:a824] error status/mask=00000001/00000000
        d = {}
        d['device'] = re.search(r'[a-z0-9]{4}:[a-z0-9]{4}', s).group()
        status_mask = re.search(
            r'error status/mask=[0-9]{8}/[0-9]{8}', s).group().split('=')[1]
        d['aer_status'],  d['aer_mask'] = status_mask.split('/')
        d['dev_id_2'] = re.search(
            r'[a-z0-9]{4}:[a-z0-9]{2}:[a-z0-9]{2}\.[a-z0-9]{1}', s).group()
        return(d)

    def parse_starting_aer_error(self, s):
        # [ 6876.924384] pcieport 0000:c0:03.3: AER: Corrected error received: 0000:c4:00.0
        d = {}
        d['error_timestamp'] = re.match(
            r'^\[ *[0-9]+\.[0-9]+\]', s).group().strip('[]').strip()
        d['dev_id_1'] = re.search(
            r'[a-z0-9]{4}:[a-z0-9]{2}:[a-z0-9]{2}\.[a-z0-9]{1}', s).group()
        _error_slot_num_1 = self.slot_map.get(d.get('dev_id_1'))
        d['error_slot_num_1'] = _error_slot_num_1 if _error_slot_num_1 else "UNKNOWN"
        d['dev_id_2'] = re.search(
            r'[a-z0-9]{4}:[a-z0-9]{2}:[a-z0-9]{2}\.[a-z0-9]{1}$', s).group()
        _error_slot_num_2 = self.slot_map.get(d.get('dev_id_2'))
        d['error_slot_num_2'] = _error_slot_num_2 if _error_slot_num_2 else "UNKNOWN"
        d['error_name'] = re.search(
            r'(?:Multiple )?Corrected error received', s).group()
        d['error_type'] = s.split(']')[1].lstrip().split(' ')[0]
        return(d)

    def parse_aer_bitfield(self, s):
        # [ 6876.943794] nvme 0000:c4:00.0: AER:    [ 0] RxErr
        d = self.parse_aer_bitfield_type_id(s)
        d['dev_id_2'] = re.search(
            r'[a-z0-9]{4}:[a-z0-9]{2}:[a-z0-9]{2}\.[a-z0-9]{1}', s).group()
        if ': AER:' in s:
            # [ 8701.632676] pcieport 0000:00:01.3: AER:    [ 6] BadTLP
            d['aer_status_bit'] = [s[s.find(': AER:    ['):].split(']')[
                0].split('[')[1].strip()]
            d['aer_status_message'] = [s[s.find(': AER:    ['):].split(']')[
                1].strip()]
        else:
            # [33283.882106] Bad TLP
            d['aer_status_message'] = s.split(']')[1].strip()

            if ('Receiver Error' or 'RxErr') in d['aer_status_message']:
                d['aer_status_bit'] = '0'
            elif ('Bad TLP' or 'BadTLP') in d['aer_status_message']:
                d['aer_status_bit'] = '6'
            elif 'Bad DLLP' or 'BadDLLP' in d['aer_status_message']:
                d['aer_status_bit'] = '7'
            elif 'Rollover' in d['aer_status_message']:
                d['aer_status_bit'] = '8'
            elif 'Timeout' in d['aer_status_message']:
                d['aer_status_bit'] = '12'
            elif 'NonFatalErr' in d['aer_status_message']:
                d['aer_status_bit'] = '13'
            else:
                d['aer_status_bit'] = ''
        return(d)

    def parse_aer_bitfield_type_id(self, s):
        # [ 6876.943794] nvme 0000:c4:00.0: AER:    [ 0] RxErr
        # Returns a dict with the following keys
        # dev_type = nvme
        # aer_dev_id = 0000:c4:00.0
        d = {}
        d['dev_id_2'] = re.search(
            r'[a-z0-9]{4}:[a-z0-9]{2}:[a-z0-9]{2}\.[a-z0-9]{1}', s).group()
        d['dev_type'] = s.split(']')[1].lstrip().split(' ')[0]
        d['aer_dev_id'] = [re.search(
            r'[a-z0-9]{4}:[a-z0-9]{2}:[a-z0-9]{2}\.[a-z0-9]{1}', s).group()]
        d['aer_slot_num'] = [self.slot_map.get(d.get('aer_dev_id')[0])]
        return(d)

def find(pattern, path):
    result = []

    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))

    return result


if __name__ == "__main__":
    main()
